# Tiny CMS

Small textfile based CMS.

## How to Install

#### using [Composer](http://getcomposer.org/)

Create a composer.json file in your project root:

```json
{
    "require": {
        "voilab/tinycms": "0.1.*"
    }
}
```

Then run the following composer command:

```bash
$ php composer.phar install
```

#### Usage

...see the code...

## Authors

[Alexandre Ravey](http://www.voilab.org)
[Joel Poulin](http://www.voilab.org)

## License

MIT Public License
