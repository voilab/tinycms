<?php
namespace voilab\tinycms;


class Element {
    const TPL_SUFFIX = '.html.twig';
    /**
     *  Nom de l'élément du site
     *
     *  @var string
     */
    private $name = '';

    /**
     *  Configuration de l'élément
     *  template: Nom du fichier de template de cet élément
     *  content_list: Fonction qui retourne la liste des éléments de ce type
     *  content: Fonction retournant le contenu d'un élément par son nom
     *
     *  @var array
     */
    private $config = array();

    /**
     *  Objet TinyCMS parent de cet élément
     *
     *  @var \voilab\tinycms\TinyCMS
     */
    private $cms = null;

    /**
     *  Route de cet élément
     *
     *  @return string
     */
    private function getRoute() {
        return '/' . $this->name;
    }

    /**
     *  Route du contenu d'un élément
     *
     *  @param  string $name nom du contenu de l'élément
     *
     *  @return string
     */
    private function getContentRoute($name) {
        return '/' . $name;
    }

    /**
     *  Contenu d'un élément, passé au parser si besoin
     *
     *  @param  string $name Nom du contenu de l'élément
     *
     *  @return array       Contenu
     */
    private function getContent($name) {
        $content = $this->config['content']($name);

        if (!$content) {
            return array();
        }

        $content = $this->parseContentValues($content);

        return $content;
    }

    private function parseContentValues($content) {
        foreach ($content as &$value) {
            if (is_array($value)) {
                $value = $this->parseContentValues($value);
            } else {
                if (strpos($value, "\n") !== false) {
                    $value = $this->cms->parse($value);
                }
            }
        }
        return $content;
    }

    /**
     *  Liste des contenus de cet élément
     *
     *  @return array Liste des contenus
     */
    private function getContentList() {
        return $this->config['content_list']();
    }

    /**
     *  Nom complet du tempate de cet élément
     *
     *  @return string
     */
    private function getTemplate() {
        return $this->config['template'] . self::TPL_SUFFIX;
    }

    /**
     *  Création d'un type d'élément du site
     *
     *  @see $config
     *  @param string         $name       Nom de l'élément
     *  @param array          $config     Configuration de l'élément
     *  @param \voilab\tinycms\TinyCMS $parent_cms Objet CMS parent
     */
    public function __construct($name, $config, $parent_cms) {
        $this->cms = $parent_cms;
        $this->name = $name;
        $this->config = array_merge_recursive($this->config, $config);
    }

    /**
     *  Enregistrement des routes gérées par cet élément et création des fonctions de rendu
     *
     *  @param  Slim $app Application Slim
     */
    public function registerRoutes($app, $secondGroup = null) {
        $group = $this->getRoute();
        if ($secondGroup) {
            $group .= '/' . trim($secondGroup, '/');
        }
        $app->group($group, function () use ($app) {
            $content_list = $this->getContentList();

            foreach ($content_list as $name => $label) {
                $app->get($this->getContentRoute($name), function() use ($app, $name) {
                    $template = $this->getTemplate();
                    $content = $this->getContent($name);
                    if (isset($content['template']) && $content['template']) {
                        $template = $content['template'] .  self::TPL_SUFFIX;
                    }
                    $app->render($template, $content);
                })->name($name);
            }
        });
    }

    /**
     *  Liste des routes gérées par cet élément
     *
     *  @return array Liste des routes et de leur labels
     */
    public function listRoutes() {
        $routes = array();
        $content_list = $this->getContentList();

        foreach ($content_list as $name => $label) {
            $routes[$name] = $label;
        }

        return $routes;
    }

    /**
     * Méthode qui récupère les contenus de toutes les routes enregistrées
     * 
     * @return array
     */
    public function getAllContents() {
        $contents = [];
        foreach (array_keys($this->listRoutes()) as $name) {
            $contents[$name] = $this->getContent($name);
        }
        return $contents;
    }
}
