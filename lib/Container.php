<?php

namespace voilab\tinycms;

class Container extends \Pimple\Container {

    /**
     * @param array $config Global configuration
     * @param mixed $engine Engine used for the Rest API
     */
    public function __construct(array $config, $engine)
    {
        parent::__construct();

        $this['config'] = array_merge(array(
            'templates_path' => '/templates',
            'parsedfiles_path' => '/userdata/contents'
        ), $config);

        $this['engine'] = $engine;

        /**
         * Markdown parser
         *
         * @return \Parsedown
         */
        $this['parsedown'] = function () {
            return new \Parsedown();
        };

        $this['fileParser'] = function ($c) {
            return new FileParser(array('data_path' => $c['config']['parsedfiles_path']));
        };

        $this['cms'] = function ($c) {
            $cms = new TinyCMS($c['config']);

            // first add a markdown parser
            $cms->registerParser(function ($content = '') use ($c) {
                return $c['parsedown']->parse($content);
            });

            return $cms;
        };
    }
}