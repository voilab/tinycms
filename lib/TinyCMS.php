<?php
namespace voilab\tinycms;

class TinyCMS {
    /**
     *  Configuration de TinyCMS
     *  templates_path: Chemin des fichiers de tempalte
     *
     *  @var array
     */
    private $config = array();

    /**
     *  Liste des éléments définis
     *
     *  @var array
     */
    private $elements = array();

    /**
     *  Navigation générale du site
     *
     *  @var array
     */
    private $navigation = array();

    /**
     *  Liste des parsers de données enregistrés
     *
     *  @var array
     */
    private $parsers = array();

    /**
     *  TinyCMS
     *
     *  @see $config
     *  @param array $config Configuration
     */
    public function __construct($config) {
        $this->config = array_merge_recursive($this->config, $config);
    }

    /**
     *  Enregistrement d'un type d'élément du site.
     *  Un élément est défini par son nom et sa configuration.
     *
     *  @param  string $element_name   Nom de l'élément
     *  @param  array  $element_config Configuration de l'élément
     *
     *  @return \voilab\tinycms\TinyCMS
     */
    public function registerElement($element_name, $element_config) {
        $element = new Element($element_name, $element_config, $this);

        if ($element) {
            $this->elements[$element_name] = $element;
        }

        return $this;
    }

    /**
     *  Ajout d'un parser de contenu.
     *  Les parsers sont executés sur le contenu dans l'ordre où ils sont ajoutés.
     *
     *  @param callable $parser_function Fonction du parser à executer
     *
     *  @return \voilab\tinycms\TinyCMS
     */
    public function registerParser($parser_function) {
        $this->parsers[] = $parser_function;

        return $this;
    }

    /**
     *  Parse un contenu avec tous les parsers enregistrés par registerParser()
     *
     *  @param  string $content Contenu
     *
     *  @return string          Contenu mise en forme
     */
    public function parse($content) {
        foreach ($this->parsers as $parser) {
            $content = $parser($content);
        }

        return $content;
    }

    /**
     *  Lecture de la config d'un élément
     *
     *  @param  string $element_name Nom de l'élément
     *
     *  @return array                Configuration de l'élément
     */
    public function getElement($element_name) {
        if (!isset($this->elements[$element_name])) {
            return null;
        }

        return $this->elements[$element_name];
    }

    /**
     *  Définition de la navigation de l'application
     *
     *  @param array  $navigation Navigation
     *
     *  @return \voilab\tinycms\TinyCMS
     */
    public function setNavigation($navigation) {
        $this->navigation = $navigation;

        return $this;
    }

    /**
     *  Lecture de la navigation complète, y compris les données des éléments.
     *
     *  @return array  Navigation
     */
    public function getNavigation() {
        $nav = array();

        foreach ($this->navigation as $key => $block) {
            $menu = $block;

            if (isset($block['element'])) {
                $element = $this->getElement($block['element']);

                if ($element) {
                    foreach ($element->listRoutes() as $name => $label) {
                        $menu['menus'][$name] = $label;
                    }
                }
            }

            $nav[$key] = $menu;
        }

        return $nav;
    }
}
