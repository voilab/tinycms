<?php
namespace voilab\tinycms;


class FileParser {
    /**
     *  Recursive Merge
     *  http://www.php.net/manual/en/function.array-merge-recursive.php#92195
     *
     *  @param   array  $array1
     *  @param   array  $array2
     *
     *  @returns array
     */
    private static function merge_recursive ( array &$array1, array &$array2 )
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value ) {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) ) {
                $merged [$key] = self::merge_recursive ( $merged [$key], $value );
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     *  Configuration du parser
     *  data_path: Chemin de base des fichiers
     *  extension: Extension des fichiers
     *
     *  @var array
     */
    private $config = array(
        'extension' => '.txt'
    );

    /**
     *  Lis le contenu d'un fichier
     *
     *  @param  string $filename Nom de la resource
     *
     *  @return string           Contenu du fichier
     */
    private function readFile($filename) {
        $filepath = $this->config['data_path'] ?: '';
        $filepath .= '/' . $filename . $this->config['extension'];

        if (!file_exists($filepath)) {
            return '';
        }

        return file_get_contents($filepath);
    }

    /**
     *  Parse une partie du contenu, un niveau à la fois
     *
     *  @param   string  $content Contenu
     *  @param   integer $depth   Niveau actuel
     *
     *  @returns array            Valeurs
     */
    private function parsePart($content, $depth = 1) {
        $elements = array();
        $content = trim($content);

        $parts = preg_split(
            '/:{' . $depth . '}([a-z0-9_-]+):{' . $depth . '}[^:]/i',
            $content,
            -1,
            PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
        );

        if (count($parts) === 0) {
            return array();
        }

        if (count($parts) < 2) {
            return $content;
        }

        for ($pos=0; $pos < count($parts); $pos += 2) {
            if (!isset($parts[$pos + 1])) {
                continue;
            }

            $value = $this->parsePart($parts[$pos + 1], $depth + 1);

            if ($parts[$pos] !== '-') {
                $elements[$parts[$pos]] = $value;
            } else {
                $elements[] = $value;
            }
        }

        return $elements;
    }

    /**
     *  Création du parser de fichiers du CMS
     *
     *  @see $config
     *  @param array $config Configuration
     */
    public function __construct($config) {
        $this->config = array_merge_recursive($this->config, $config);
    }

    /**
     *  Parse un fichier et retourne son contenu indexé dans un tableau
     *
     *  @param  string $filename Nom du fichier
     *
     *  @return array           Contenu indexé
     */
    public function parseFile($filename) {
        return $this->parsePart(
            $this->readFile($filename)
        );
    }

    /**
     *  Parse plusieurs fichiers et remplace les valeurs du précédent par le suivant
     *
     *  @param   array $filenames  Liste des fichiers
     *
     *  @returns array             Contenu indexé
     */
    public function parseFiles($filenames) {
        $elements = array();

        foreach ($filenames as $filename) {
            $elements = self::merge_recursive(
                $elements,
                $this->parseFile($filename)
            );
        }

        return $elements;
    }
}
